# 2018 Apereo Fellow nomination for Tim Vertein

+ Nominee’s Name: Tim Vertein
+ Nominee’s Email: timothy.vertein@wisc.edu
+ Nominee’s Organization: University of Wisconsin / MyUW

cf. [last year's nomination][].

## Past Contributions

[Tim is a uPortal Committer][] with
[54 commits to the main uPortal repo since 2012][uPortal contributors 2012-now].

Technical contributions include:

+ Adding portlet publication lifecycle state metadata to the uPortal layout JSON
  API ([PR 815][])
+ Improving layout reset to work across profiles ([PR 672][])
+ Adding another strategy for profile mapping ( [PR 668][])
+ Much of the initial implementation of the "marketplace" portlet browsing
  feature; some of the initial implementation of the "favorites" feature
+ Bugfixes including to sticky profile selection; refactoring including to
  XSLTs; maintenance including dependency updates and test case maintenance.

Tim is also a uPortal-home and
[uPortal-app-framework committer][] with
[130 (4th) commits][uPortal-home contributions to date] and
[105 (3rd) commits][uPortal-app-framework contributions to date] to date. Note
that commit counts are a notoriously flawed measure of contribution.
Nonetheless these are an indicator of the evident reality, that Tim has
contributed significantly to the `uPortal` repository and is a technical leader
in the `uPortal-app-framework` and `uPortal-home` incubating project developing
an optional client-side user experience enhancing uPortal.

The `uPortal-home` and `uPortal-app-framework` projects are an important part of
uPortal's strategy to remain relevant and adoptable in the face of evolving
expectations of portal experiences supporting education.

Tim's technical contributions in and around `uPortal-home` include:

+ panache in the search feature
+ excellence in widgets feature
+ panache in the app directory feature, including ratings and reviews features
+ nuances in the experience when not authenticated
+ attention to detail in Google Analytics integration
+ bugfixes
+ documentation and configurability
+ refactoring
+ release engineering and dependency updating

Tim has contributed to the core-to-uPortal Web Proxy Portlet and the
incidentally open source UW-Madison HRS Portlets, in the former providing:

+ support for presenting user attributes as headers to the proxied resources
+ bug fixes
+ release engineering

Tim has been essential to the he growing slate of open source microservices
supporting `uPortal-home` and available for implementation in other contexts.

Tim has been an active Apereo conference presenter and participant since his
entering the Apereo uPortal scene with the 2014 Apereo conference. He hit the
ground running facilitating social activities, has served on conference
committees and in background roles contributing to proposal review, and has
presented several well-regarded and well-attended conference sessions including
pre-conference seminars, on Design Sprints, and on Material Design. Tim is
an exceptionally welcoming and thoughtful individual participant passionate
about public service and fiercely loyal to the University in the best traditions
of Apereo and of Jasig before it.

+ "MyUW for You" pre-conference workshop (2017)
+ "MyUW and You" presentation (2017)
+ "Material Design" (2017)
+ "Design Sprints" (2015)
+ "AngularJS client of uPortal web services : hands on with MyUW Beta" (2015)
+ Apereo Portlet Showcase (2014)
+ Developing a Sophisticated Portlet: New Version of Web Conferencing Portlet as
  Case Study (2014)
+ Next Generation Portal: Redesigning the portal from a user-focused,
  responsive, mobile-first perspective (2014)

Tim was essential to MyUW's hosting a uPortal developer meetup in 2017, going
above and beyond to organize and facilitate this event. The event was successful
because of Tim's efforts: because of these efforts it was appropriately planned
and prepared; without these efforts it would have been less effective for its
chaos. This meetup was the most significant between-conferences activity for
uPortal in 2017.

Tim serves as [Scrum Master][] and [Development Lead][] in MyUW, the
University of Wisconsin's local uPortal implementation serving the entire UW
System. This is a flagship implementation for the uPortal open source project.
In these roles Tim is essential to the continued success of this major uPortal
implementation which in turn is an important factor in the continued success and
viability of the uPortal open source project.

In these roles Tim regularly reaches out via the uPortal email lists to update
the community on MyUW progress and the attendant surfaces for collaboration and
engagement.

Tim is a significant factor in the team success of MyUW. This is only a summary
of some of Tim's individual contributions, but were it exhaustive, that would
still be kind of missing the point: much of Tim's role as Scrum Master,
Development Lead, and even (especially) Committer is in facilitating, coaching,
supporting, enabling the efforts of contributors and teams in delivering
clarifying participant experiences of education's complex online environment, in
part through the development and advancement of free and open source software
collaboratively developed in education.

## Expected Future Contributions

Tim has concrete intentions to attend the upcoming Open Apereo 2018 conference,
with an accepted presentation there.

I expect Tim will continue to serve as a Committer in the `uPortal-home`,
`uPortal-app-framework`, and `uPortal` core projects. I further expect `-home`
and `-app-framework` to remain an important part of uPortal's claim to continued
relevance.

I expect Tim will continue to serve as the ScrumMaster and Development Lead in
the MyUW project. He's certainly adding a lot of value in these roles and is key
to MyUW's success. There is of course always the risk that he'll grow and
promote out of these roles -- this is what tends to happen to highly capable,
effective contributors.

I expect MyUW and Apereo will continue to be a better, more effective, more
thoughtful place for Tim's participation and influence.

[vertein commits to AngularJS-portal master]: https://github.com/UW-Madison-DoIT/angularjs-portal/commits?author=vertein
[vertein commits to uPortal master]: https://github.com/Jasig/uPortal/commits?author=vertein
[vertein commits to uw-frame master]: https://github.com/UW-Madison-DoIT/uw-frame/commits/master?author=vertein
[vertein commits to WebProxyPortlet master]: https://github.com/Jasig/WebProxyPortlet/commits/master?author=vertein
[PR 668]: https://github.com/Jasig/uPortal/pull/668
[PR 672]: https://github.com/Jasig/uPortal/pull/672
[PR 815]: https://github.com/Jasig/uPortal/pull/815
[last year's nomination]: https://gist.github.com/apetro/b252ee3c0d73105c9021f0e7412f14c3
[Tim is a uPortal Committer]: https://github.com/Jasig/uPortal/blob/master/docs/COMMITTERS.md#who-are-the-committers-github-username
[uPortal contributors 2012-now]: https://github.com/Jasig/uPortal/graphs/contributors?from=2013-01-01&to=2018-03-09&type=c
[Scrum Master]: https://docs.google.com/document/d/132sGQGK_bXF_R26C8WOj-Nq4Sh4P9Sx0Q5qvIJsZHIU/edit#heading=h.x1z0o3vrwjzc
[Development Lead]: https://docs.google.com/document/d/132sGQGK_bXF_R26C8WOj-Nq4Sh4P9Sx0Q5qvIJsZHIU/edit#heading=h.rz37ht7zd2sq
[uPortal-app-framework committer]: https://github.com/uPortal-Project/uportal-app-framework/blob/master/committers.md#who-are-the-committers
[uPortal-home contributions to date]: https://github.com/uPortal-Project/uportal-home/graphs/contributors?from=2014-08-17&to=2018-03-09&type=c
[uPortal-app-framework contributions to date]: https://github.com/uPortal-Project/uportal-app-framework/graphs/contributors?from=2015-08-04&to=2018-03-09&type=c
